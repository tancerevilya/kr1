import time
import fileinput
import os

def in_prog():
    kadr = [] #кадр
    z = '' #временная переменная
    for strk in fileinput.input('earth.txt'):
        if strk.startswith('#') or strk.startswith('\n'):
            continue
        if strk.startswith('```') and len(z) == 0:
            continue
        if strk.startswith('```') and len(z) > 0:
            kadr.append(z)
            z = ''
            continue
        else:
            z += strk
    return kadr

def in_scr(kadr):
    os.system('cls') 
    r = 0
    while r < len(kadr):
        print(kadr[r])
        time.sleep(0.4)
        if r == len(kadr) - 1:
            r = 0
        else:
            r += 1

if __name__ == '__main__':
    try:
        in_scr(in_prog())
    except KeyboardInterrupt:
        print('\033[0m'+'end')

