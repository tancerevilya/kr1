import math

n = int(input('Введите скалярную величину: '))
print()
a = [[8, 2, 1], [9, 5, 3], [7, 4, 6]]
print('Изначальная 1 матрица')
print(a)
print('Перемноженная 1 матрица ')
for i in range(3):
    for j in range(3):
        a[i][j]*=n
        print(a[i][j], end='  ')
    print('')
print()

b = [[5, 3, 1], [6, 8, 2], [7, 9, 0]]
print('Изначальная 2 матрица')
print(b)
print('Сумма 1 и 2 матрицы')
for i in range(3):
    for j in range(3):
        b[i][j]+=a[i][j]
        print(b[i][j], end='  ')
    print('')
