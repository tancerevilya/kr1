import math
import time

a = []
n1 = int(input('Введите количество строк для первого массива: ')) #строка
m1 = int(input('Введите количество столбцов для первого массива: ')) #столб
for i in range(n1):
    p1 = []
    for i in range(m1):
        p1.append(int(input('Введите переменные: ')))
    a.append(p1)
for i in a:
    print(i)

b = []
n2 = int(input('Введите количество строк для второго массива: ')) #строка
m2 = int(input('Введите количество столбцов для второго массива: ')) #столб
for i in range(n2):
    p2 = []
    for i in range(m2):
        p2.append(int(input('Введите переменные: ')))
    b.append(p2)
for i in b:
    print(i)

start_time = time.time()
p = []
for i in range(n1):
    for j in range(m1):
        elem = 0
        for r in range(n1):
            elem = elem + a[i][r]*b[r][j]
        p.append(elem)
print('Произведение матриц:')
print(p)
print("--- %s seconds ---" % (time.time() - start_time))

